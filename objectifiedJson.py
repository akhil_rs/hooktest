import simplejson as json
from lxml import objectify
import json

class objectJSONEncoder(json.JSONEncoder):
       model = (
        (objectify.IntElement, int),
        (objectify.NumberElement, float),
        (objectify.FloatElement, float),
        (objectify.ObjectifiedDataElement,
            lambda s: str(s).strip().encode('utf-8')),
    )

    def default(self,o):
        for o_type, constructor in self.model:
            if isinstance(o, o_type):
                return constructor(o)
        if hasattr(o, '__dict__'):
            #For objects with a __dict__, return the encoding of the __dict__
            return [item.__dict__ for item in o]
        return json.JSONEncoder.default(self, o)

obj = objectify.fromstring("<Book><price>1.50</price><author>W. Shakespeare</author></Book>")
return json.dumps(obj, cls=objectJSONEncoder)