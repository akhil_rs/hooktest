import datetime
import json

from pymongo import MongoClient

uri = "mongodb://mongo-admin:4jz24meWykJ4NZ2N@60.20.0.208:38109,\
60.20.40.149:38109,60.20.0.223:38109/rcmb_qa?authSource=admin&\
replicaSet=rcmbrain"
client = MongoClient(uri)
clientdb = client["rcmb_qa"]
# uri = "mongodb://mongo-admin:hzO7qFI0d0TivIGo@\
# 60.40.20.112:59658,60.40.20.149\
# :59658,60.40.10.70:59658/admin?authSource=admin&replicaSet=rcmbrain"
# client = MongoClient(uri)
# clientdb = client['rcmb_prod']


dReplaceBy = {
    "seq": "Sequence",
    "dob": "Date Of Birth",
    "clm": "Claim",
    "chrg": "Charge",
    "diag": "Diagnosis",
    "pymt": "Payment",
    "adjsmnt": "Adjustment",
    "amnt": "Amount",
    "seq": "Sequence",
    "num": "Number",
    "gen": "General",
    "pm": "Patient Management",
    "ptnt": "Patient",
    "qual": "Qualifier",
    "trans": "Transaction",
    "dos": "Date Of Service",
    "wfName": "Work Flow Name",
    "wfId": "Work Flow Id",
    "frq": "Frequency",
}


def get_field_display_name(sFieldName):
    lWords = sFieldName.split("_")
    sFieldDisplayName = " ".join(lWords)
    for k, v in dReplaceBy.items():
        sFieldDisplayName = sFieldDisplayName.replace(k, v)
    sFieldDisplayName = sFieldDisplayName.title()
    return sFieldDisplayName


DATA_TYPE_INTEGER = "integer"
DATA_TYPE_STRING = "string"
DATA_TYPE_BOOLEAN = "boolean"
DATA_TYPE_DATE = "date"
DATA_TYPE_NUMBER = "number"
DATA_TYPE_DATETIME = "datetime"

DATA_TYPES = {
    DATA_TYPE_INTEGER: int,
    DATA_TYPE_STRING: str,
    DATA_TYPE_BOOLEAN: bool,
    DATA_TYPE_DATE: datetime.date,
    DATA_TYPE_NUMBER: float,
    DATA_TYPE_DATETIME: datetime.datetime,
}


def field_type_check(value_):
    for key, each in DATA_TYPES.items():
        # print(isinstance(value_,each))
        if isinstance(value_, each):
            return key


schema_dict = {"name": "master_db_schema"}
name = "master_db_schema"
collections = list(clientdb.collection_names())
value_ = []

for collection in collections:
    print(collection)
    if collection == "system.views":
        continue
    cursor = clientdb[collection].find_one({})
    print(cursor)
    data = {}
    if not cursor:
        continue
    collection_field_dtls = []
    foreign_key_dts = []

    for key, value in cursor.items():
        type_ = field_type_check(value)
        # print(type_,value)
        if key != "_id":
            # print("k1",k1)
            k1 = get_field_display_name(key)
            d = {"label": k1, "name": key, "type": type_}
            collection_field_dtls.append(d)
    # foreign_key_dts.append({
    #           "field":None,
    #           "reference":{
    #           	 "field":None,
    #              "resource":None
    #           }
    #        })
    uniqueKeys = []

    data = {
        "fields": collection_field_dtls,
        "foreignKeys": foreign_key_dts,
        "primaryKey": [],
        "required": [],
        "uniqueKeys": uniqueKeys,
    }
    # print("data",data)
    k2 = get_field_display_name(collection)
    value_.append({"label": k2, "name": collection, "schema": data})
# print("value",value)

dict_ = {"name": "master_db_schema", "value": value_}
# print(type(dict_))
with open("./schema1.json", "w") as fp:
    json.dump(dict_, fp)
